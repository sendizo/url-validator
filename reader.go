package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
)

func UrlCsvReader(file string) []string {
	f, err := os.Open(file)
	if err != nil {
		fmt.Println("Error Opening File")
	}
	defer f.Close()

	// Line Reader
	nb, _ := LineCounter(file)
	store := make([]string, nb)
	//
	r := csv.NewReader(f)
	//
	for i := int64(0); i < nb; i++ {
		row, _ := r.Read()
		store[i] = row[0]
	}
	return store
}

func LineCounter(file string) (int64, error) {
	f, err := os.Open(file)
	if err != nil {
		fmt.Println("Error Opening File")
	}
	defer f.Close()
	// Counter
	lc := int64(0)
	s := bufio.NewScanner(f)
	for s.Scan() {
		lc++
	}
	return lc, s.Err()
}
