package main

import (
	"net/http"
	"strings"
)

const (
	ValidURL    = "Valid URL"
	InvalidURL  = "Bad URL"
	BadDomain   = "Bad Domain"
	BadProtocol = "Bad Protocol"
)

type UrlResults map[string]string

func CheckWebsite(url []string) UrlResults {
	// Slice lenght
	nb := len(url)
	// Make Map
	results := make(map[string]string, nb)
	// Scanning
	for i := 0; i < nb; i++ {
		response, err := http.Head(url[i])
		if err != nil {
			if strings.Contains(err.Error(), "no such host") {
				results[url[i]] = BadDomain
				continue
			}
			if strings.Contains(err.Error(), "unsupported protocol") {
				results[url[i]] = BadProtocol
				continue
			}
			if strings.Contains(err.Error(), "no Host in request URL") {
				results[url[i]] = BadProtocol
				continue
			}
		}
		if response.StatusCode != http.StatusOK {
			results[url[i]] = InvalidURL
			continue
		}
		results[url[i]] = ValidURL
	}
	return results
}

func UrlCheck(url string) string {
	response, err := http.Head(url)
	if err != nil {
		if strings.Contains(err.Error(), "no such host") {
			return BadDomain
		}
		if strings.Contains(err.Error(), "unsupported protocol") {
			return BadProtocol
		}
	}
	if response.StatusCode != http.StatusOK {
		return InvalidURL
	}
	return ValidURL
}
